var svgController;
var svg;
var documentInfo = {visible: false, topOfDocument: false, rightOfDocument: false, docX: Infinity, docY: Infinity};
var docMarkerReferencePoint;
var tileSize = 50;
var docPositions = [];
var clickDeviation = 10;
var docIndicatorWidth = Infinity;
var docIndicatorHeight = Infinity;
var transitionId;
var panTransitionTime = 500;
var panTransitionStep;
var panTransitionVector;
var nothingFoundTimoutId = null;
var interactionHappened = false;
var oldZoom = 1;


function onInteraction(e) {
	if(oldZoom == svgController.getZoom()) {
		interactionHappened = true;
	} else {
		interactionHappened = false;
		oldZoom = svgController.getZoom();
	}
	updateDocumentsVisibility();

	if(documentInfo.visible) {
		updateDocumentInfoPosition();
	}
}

function updateDocumentsVisibility() {
	var documentVisibilitySelection = document.getElementById("documentVisibility").value;
	var render = true;

	if(documentVisibilitySelection == "far") {
		if(svgController.getZoom() < 3) {
			render = false;
		}
	} else if(documentVisibilitySelection == "middle") {
		if(svgController.getZoom() < 6) {
			render = false;
		}
	} else if(documentVisibilitySelection == "near") {
		if(svgController.getZoom() < 9) {
			render = false;
		}
	}

	if(render) {
		svg.getElementById("documents").style.display = "inline";
		updateDocumentTilesVisibility();
	} else {
		svg.getElementById("documents").style.display = "none";
	}
}

function updateDocumentTilesVisibility() {
	//Render only the tiles, that are visible
	var svgWidth = svg.getElementsByTagName("svg")[0].width.baseVal.value;
	var svgHeight = svg.getElementsByTagName("svg")[0].height.baseVal.value;

	var visibleSection = {};
	visibleSection.x = Math.max(svgController.getPan().x * -1 / svgController.getZoom(), 0);
	visibleSection.y = Math.max(svgController.getPan().y * -1 / svgController.getZoom(), 0);
	visibleSection.x2 = Math.min(svgController.getPan().x * -1 / svgController.getZoom() + document.getElementById("svgMap").getBoundingClientRect().width / svgController.getZoom(), svgWidth);
	visibleSection.y2 = Math.min(svgController.getPan().y * -1 / svgController.getZoom() + document.getElementById("svgMap").getBoundingClientRect().height / svgController.getZoom(), svgHeight);

	for (var x = 0; x < Math.ceil(svgWidth / tileSize); x++) {
		for (var y = 0; y < Math.ceil(svgHeight / tileSize); y++) {
			var tileSection = {};
			tileSection.x = x*tileSize;
			tileSection.y = y*tileSize;
			tileSection.x2 = x*tileSize + tileSize;
			tileSection.y2 = y*tileSize + tileSize;

			if(visibleSection.x <= tileSection.x2 && visibleSection.x2 >= tileSection.x && visibleSection.y <= tileSection.y2 && visibleSection.y2 >= tileSection.y) {
				svg.getElementById("docTile_" + x + "_" + y).style.display = "inline";
			} else {
				svg.getElementById("docTile_" + x + "_" + y).style.display = "none";
			}
		};
	};
}

function getDocumentPositionsFromSvg(svg) {
	var docs = svg.getElementsByClassName("docIndicators");

	docIndicatorWidth = docs[0].width.baseVal.value;
	docIndicatorHeight = docs[0].height.baseVal.value;

	for(var i = 0; i < docs.length; i++) {
		var x = docs[i].x.baseVal.value + docs[i].width.baseVal.value*0.5;
		if(typeof docPositions[x] === "undefined") {
			docPositions[x] = []
		}

		docPositions[x].push({y: docs[i].y.baseVal.value + docs[i].height.baseVal.value*0.5, id: docs[i].id})
	}
}

function onClick(e) {
	if(document.getElementById("documentInfo").style.visibility == "visible") {
		return;
	}

	//Get the relative position of the cursor in the svg
	var marker = document.getElementById("docMarker")
	var mouse = {x: (parseFloat(marker.style.left) + e.clientX - svgController.getPan().x) / svgController.getZoom(), y: (parseFloat(marker.style.top) + e.clientY - svgController.getPan().y) / svgController.getZoom()};

	//Get closest documents
	var closestDocuments = getClosestDocuments(mouse);

	if(closestDocuments.ids.length > 0) {
		docMarkerReferencePoint = {x: closestDocuments.x, y: closestDocuments.y};
		showInfoBox(closestDocuments);
	}
}

function showInfoBox(closestDocuments, highlightedId) {
	//If at least one document was close enought show the info box
	if(closestDocuments.ids.length > 0) {
		//Show the info box
		document.getElementById("documentInfo").style.visibility = "visible";

		if(typeof document.getElementById("documentInfo").scrollTo === "function") {
			document.getElementById("documentInfo").scrollTo(0,0);
		}
		documentInfo.visible = true;
		documentInfo.docX = closestDocuments.x;
		documentInfo.docY = closestDocuments.y;

		var infoText = "";
		for(var i = 0; i < closestDocuments.ids.length; i++) {
			infoText += "<p>";
			var meta = $("#" + closestDocuments.ids[i]);
			//Set the text for the info
			var path = meta.attr("path");
			var real_path = path.replace("htdocs/", "")
			real_path = real_path.replace("-lemmatized", "")
			var title = meta.attr("title")

			var styleClass = ""
			if(closestDocuments.ids[i] == highlightedId) {
				styleClass = "highlightedTitle"
			}
			infoText += "<a class=\"" + styleClass + "\" href='"+ real_path +"'>"+ title +"</a>";
			infoText += "</p>";
		}
		document.getElementById("documentInfoText").innerHTML = infoText;

		//Determine the right position for the document info field
		var svgBB = document.getElementById("svgMap").getBoundingClientRect();
		var documentInfoBB = document.getElementById("documentInfo").getBoundingClientRect();
		var docScreenX = closestDocuments.x * svgController.getZoom() + svgController.getPan().x;
		var docScreenY = closestDocuments.y * svgController.getZoom() + svgController.getPan().y;



		if(docScreenX + documentInfoBB.width < svgBB.width) {
			documentInfo.rightOfDocument = true;
		} else {
			documentInfo.rightOfDocument = false;
		}

		if(docScreenY - documentInfoBB.height - 5 > 0) {
			documentInfo.topOfDocument = true;
		} else {
			documentInfo.topOfDocument = false;
		}

		//Position the document info
		updateDocumentInfoPosition();
	} else {
		//Hide the info box
		document.getElementById("documentInfo").style.visibility = "hidden";
		documentInfo.visible = false;
	}
}

function updateDocumentInfoPosition() {
	var docScreenX = documentInfo.docX * svgController.getZoom() + svgController.getPan().x;
	var docScreenY = documentInfo.docY * svgController.getZoom() + svgController.getPan().y;
	var docScreenWidth = docIndicatorWidth * svgController.getZoom();
	var docScreenHeight = docIndicatorHeight * svgController.getZoom();
	var documentInfoBB = document.getElementById("documentInfo").getBoundingClientRect();

	if(documentInfo.rightOfDocument) {
		document.getElementById("documentInfo").style.left = (docScreenX - docScreenWidth*0.5) + "px";
	} else {
		document.getElementById("documentInfo").style.left = (docScreenX + docScreenWidth*0.5 - documentInfoBB.width) + "px";
	}

	if(documentInfo.topOfDocument) {
		document.getElementById("documentInfo").style.top = (docScreenY - documentInfoBB.height - docScreenHeight) + "px";
	} else {
		document.getElementById("documentInfo").style.top = (docScreenY + docScreenHeight*1) + "px";
	}

	markDocument(docMarkerReferencePoint);
}

function onMouseMove(e) {
	if(document.getElementById("documentInfo").style.visibility == "visible") {
		return;
	}

	var closestDocuments = getClosestDocuments({x: (e.clientX - svgController.getPan().x) / svgController.getZoom(), y: (e.clientY - svgController.getPan().y) / svgController.getZoom()});

	if(closestDocuments.ids.length > 0) {
		markDocument({x: closestDocuments.x, y: closestDocuments.y});
	} else {
		document.getElementById("docMarker").style.visibility = "hidden";
	}
}

function markDocument(docPos) {
		var docScreenX = docPos.x * svgController.getZoom() + svgController.getPan().x;
		var docScreenY = docPos.y * svgController.getZoom() + svgController.getPan().y;
		var docMarkerBB = document.getElementById("docMarker").getBoundingClientRect();

		document.getElementById("docMarker").style.left = (docScreenX - docMarkerBB.width*0.5) + "px";
		document.getElementById("docMarker").style.top = (docScreenY - docMarkerBB.height*0.5) + "px";
		document.getElementById("docMarker").style.visibility = "visible";
}

function getClosestDocuments(mouse) {
	//Find the documents nearest to the cursor
	var shortestDist = Infinity;
	var poweredCD = Math.pow(clickDeviation, 2);
	var closestDocuments = {x: Infinity, y: Infinity, ids: []};

	for(var x in docPositions) {
		//Check if objects with this x-value can be within the click radius
		if(Math.pow(mouse.x - x, 2) < poweredCD) {
			for(var i = 0; i < docPositions[x].length; i++) {
				var y = docPositions[x][i].y;
				var dist = Math.pow(mouse.x - x, 2) + Math.pow(mouse.y - y, 2);

				if(dist < Math.min(poweredCD, shortestDist)) {
					shortestDist = dist;
					closestDocuments.x = x;
					closestDocuments.y = y;

					closestDocuments.ids = [];
					closestDocuments.ids.push(docPositions[x][i].id);
				} else if(dist == Math.min(poweredCD, shortestDist)) {
					closestDocuments.ids.push(docPositions[x][i].id);
				}
			}
		}
	}

	return closestDocuments;
}

function searchDocument(e) {
	document.getElementById("nothingFound").style.display = "none";

	if(e == null || e.keyCode == 13) {
		var docName = document.getElementById("searchInput").value;

		var metas = document.getElementById("hidden").getElementsByTagName("li");
		var docId = null;

		for(var i = 0; i < metas.length; i++) {
			var filename = metas[i].getAttribute("path").substring(metas[i].getAttribute("path").lastIndexOf("/")+1);

			if(filename === docName) {
				docId = metas[i].getAttribute("id");
				break;
			}
		}

		if(docId != null) {
			//Search document poistion
			for(var x in docPositions) {
				for(var i = 0; i < docPositions[x].length; i++) {
					if(docPositions[x][i].id == docId) {
						var docPos = {x:x, y: docPositions[x][i].y};
						docMarkerReferencePoint = docPos;
						markDocument(docPos);
						showInfoBox(getClosestDocuments(docPos), docPositions[x][i].id);

						//Transition the map to the found document
						var docScreen = {x: x * svgController.getZoom() + svgController.getPan().x, y: docPositions[x][i].y * svgController.getZoom() + svgController.getPan().y};
						var svgBB = document.getElementById("svgContainer").getBoundingClientRect();
						var mapScreenCenter;
						if(isNaN(svgBB.x)) {
							mapScreenCenter = {x: svgBB.left + svgBB.width*0.5, y: svgBB.top + svgBB.height*0.5};
						} else {
							mapScreenCenter = {x: svgBB.x + svgBB.width*0.5, y: svgBB.y + svgBB.height*0.5};
						}
						panTransitionVector = {x: mapScreenCenter.x - docScreen.x, y: mapScreenCenter.y - docScreen.y};
						var panTransitionVectorLen = Math.sqrt(Math.pow(panTransitionVector.x, 2) + Math.pow(panTransitionVector.y, 2));
						panTransitionStep = panTransitionVectorLen / panTransitionTime * 16;

						transitionId = setInterval(moveMapTo, 16);

						break;
					}
				}
			}
		} else {
			document.getElementById("nothingFound").style.display = "block";

			if(nothingFoundTimoutId != null) {
				clearTimeout(nothingFoundTimoutId);
			}
			nothingFoundTimoutId = setTimeout(function() {document.getElementById("nothingFound").style.display = "none";}, 10000);
		}

		return false;
	} else {
		return true;
	}
}

function moveMapTo() {
	var panTransitionVectorLen = Math.sqrt(Math.pow(panTransitionVector.x, 2) + Math.pow(panTransitionVector.y, 2));
	var stepVector = {x: panTransitionVector.x / panTransitionVectorLen * panTransitionStep, y: panTransitionVector.y / panTransitionVectorLen * panTransitionStep};

	var transitionFinished = false;
	if((panTransitionVector.x < 0 && panTransitionVector.x > stepVector.x) || (panTransitionVector.x > 0 && panTransitionVector.x < stepVector.x)) {
		stepVector = panTransitionVector;
		transitionFinished = true;
	}

	svgController.panBy(stepVector);
	panTransitionVector = {x: panTransitionVector.x - stepVector.x, y: panTransitionVector.y - stepVector.y};
	

	if(transitionFinished) {
		clearInterval(transitionId);
		interactionHappened = false;
	}
}

function onClickSvgMap() {
	if(!interactionHappened) {
		document.getElementById("documentInfo").style.visibility = "hidden";
		documentInfo.visible = false;

		document.getElementById("docMarker").style.visibility = "hidden";
	} else {
		interactionHappened = false;
	}
}

function onLoad() {
	svg = document.getElementById("svgMap").contentDocument;
	svg.onclick = onClickSvgMap;
	document.getElementById("svgDocMarker").contentDocument.onclick = onClick;
	svg.addEventListener ("mousemove", onMouseMove, false);

	getDocumentPositionsFromSvg(svg);

	svgController = svgPanZoom("#svgMap", {
		zoomEnabled: true,
		controlIconsEnabled: false,
		center: true,
		fit: false,
		onPan: onInteraction,
		dblClickZoomEnabled: false,
		maxZoom:12,
		minZoom: .2
	});
}

window.addEventListener("load", onLoad);
