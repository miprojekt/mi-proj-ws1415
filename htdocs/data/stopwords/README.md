

English
=======

Source of `common-english-words.txt`: <http://www.textfixer.com/resources/common-english-words.txt>

Source of `ranks-nl.txt`: <http://www.ranks.nl/stopwords> (merged lists and removed duplicates)

Source of `lextek-com.txt`: <http://www.lextek.com/manuals/onix/stopwords1.html>

Source of `webconfs-com.txt`: <http://www.webconfs.com/stop-words.php>


German
======

Source of `ehtio-de.txt`: <http://www.ehtio.de/downloads/deutsche-stoppwoerterliste.txt>

Source of `infowiss-net.txt`: <http://wiki.infowiss.net/Stoppwort>

Source of `phpbar-de.txt`: <http://www.phpbar.de/w/Stoppwortliste_deutsch>

Source of `solariz-de.txt`: <https://solariz.de/de/deutsche_stopwords.htm>

Source of `top10-listen-ch.txt`: <http://www.top10-listen.ch/top-listen/internet/1803-deutsche-stopwords/>
