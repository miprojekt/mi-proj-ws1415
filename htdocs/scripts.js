
function doAJAX(){
	$.getJSON("topics.json", function(data) {
		for (var i = 0; i < data.length; i++) {
			var x = $("<option>" + data[i] + "</option>");
			var y = $("<option>" + data[i] + "</option>");
			$("#dimension-x").append(x);
			$("#dimension-y").append(y);
		}
	});
}

$(document).ready(function() { doAJAX(); } );

function reloadTopics(){
	var x = $("#dimension-x").prop("selectedIndex");
	var y = $("#dimension-y").prop("selectedIndex");
	filename = "_build-all/" + x + "-" + y + ".svg";
	$("#svgMap").attr("data", filename);
	// FIXME works only if svg loads in <500ms
	setTimeout(function() { onLoad(); }, 500);
}

function loadMeta(){
	$.getJSON("titles.json", function(data) {
		for (var key in data) {
			var x = $("<li id='"+key+"' title='"+data[key][0]+"' path='"+data[key][1]+"'>" + data[key] + "</li>");
			$("#hidden").append(x);
		}
	});
}

$(document).ready(function() { loadMeta(); } );
