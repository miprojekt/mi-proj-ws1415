#!/usr/bin/python
# -*- coding: utf-8 -*-

import subprocess
import argparse
import time
import logging
import os
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
parser = argparse.ArgumentParser(description="measure topicmapgeneration")
parser.add_argument("-o", "--output", help="Output file", default="test-result.csv", required=False)
parser.add_argument("-p", "--prefix", help="prefix to result-storage", default="", required=False)
parser.add_argument("-s", "--short", help="quick test", action="store_true")
args = parser.parse_args()
outputfile = args.output

strategies = [0, 1, 2]
documents = [10,25,50,100,500,1000,5000,10000,20000,22000]
iterations = 200

if args.short:
	documents = [100]
	outputfile = outputfile.replace(".csv", "-short.csv")
	iterations = 50

HEADER = "return_code, duration, collection_name, documents, placement, iterations"

def checkfile(filename):
	if not os.path.exists(filename):
		with open(filename, "w") as output:
			output.write(HEADER)
			output.write("\n")

def call(collection, documents, placement, iterations):
	collection_name = collection[1]
	output = str(args.prefix)+str(placement) +"-"+ str(documents) + ("-short" if args.short else "") +"-"+ collection_name
	starttime = time.time()
	return_code = subprocess.call(["python", "topic_map_generator.py", "-n", str(documents), "-p", str(placement), "-i", str(iterations), "-o", output, 
	#, "-c", str(collection)
	])
	endtime = time.time()
	duration = endtime - starttime
	return (return_code, duration, collection_name, documents, placement, iterations)

if __name__=="__main__":
	checkfile(outputfile)
	with open(outputfile, "a") as output:
		for s in strategies:
			for d in documents:
				logging.info("strategy "+str(s)+"; "+str(d)+" documents")
				result = call((1,"reuters","htdocs/data/reuters","Die Reuters Kollektion"), documents=d, placement=s, iterations=iterations )
				output.write(",".join(map(str,result)) + "\n")
				output.flush()
				logging.info(str(result))

