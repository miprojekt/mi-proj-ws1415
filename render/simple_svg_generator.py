import math
from helper import config


CONST_DOCUMENT_INDICATOR_RADIUS = 1

# doc_coords format: [[x0,y0],[x1,y1],[x2,y2],...]

# doc_ids: string array
def generate_svg(doc_coords, doc_ids=None, fixed_size=False):
	dimensions = get_dimensons(doc_coords, fixed_size)

	svg = svg_header(dimensions)

	tiles = create_tiles(doc_coords, dimensions, doc_ids)

	svg += write_document_tiles_to_svg(tiles)
	svg += "\n</svg>"
	return svg

# returns the highest coordinate values
def get_dimensons(doc_coords, fixed_size=False):
	if not fixed_size is False:
		return fixed_size
	cur_dimension_x = 0;
	cur_dimension_y = 0;
	for coord in doc_coords:
		if(coord[0] > cur_dimension_x):
			cur_dimension_x = coord[0]

		if(coord[1] > cur_dimension_y):
			cur_dimension_y = coord[1]
	cur_dimension_x += CONST_DOCUMENT_INDICATOR_RADIUS*2
	cur_dimension_y += CONST_DOCUMENT_INDICATOR_RADIUS*2

	return [math.ceil(cur_dimension_x), math.ceil(cur_dimension_y)]

# sorts the documents into tiles
def create_tiles(doc_coords, dimensions, doc_ids=None, tile_size=50):
	num_tiles = [int(math.ceil(dimensions[0]/tile_size)), int(math.ceil(dimensions[1]/tile_size))]

	tiles = []
	for x in xrange(0,num_tiles[0]):
		tiles.append([])
		for y in xrange(0, num_tiles[1]):
			tiles[x].append([])

	for i in xrange(len(doc_coords)):
		if doc_ids == None:
			document_id = "foo"
		else:
			document_id = doc_ids[i]

		doc_coords_x = doc_coords[i][0] + CONST_DOCUMENT_INDICATOR_RADIUS
		doc_coords_y = doc_coords[i][1] + CONST_DOCUMENT_INDICATOR_RADIUS

		tiles[int(math.floor(doc_coords_x/tile_size))][int(math.floor(doc_coords_y/tile_size))].append([doc_coords_x, doc_coords_y,document_id])


	return tiles


# writes the previously created document tiles to the svg
def write_document_tiles_to_svg(tiles):
	svg = "<g id=\"documents\">\n"

	document_counter = 0

	for x in xrange(len(tiles)):
		for y in xrange(len(tiles[x])):
			svg += "\t<g id=\"docTile_" + str(x) + "_" + str(y) + "\">\n"

			for cur_doc in tiles[x][y]:
				svg += "\t\t<circle cx=\"" + str(cur_doc[0]) + "\" cy=\"" + str(cur_doc[1]) + "\" r=\"" + str(CONST_DOCUMENT_INDICATOR_RADIUS) +"\" "
				svg += "id=\"doc" + str(document_counter) + "\" onclick=\"onDocumentClick('doc" + str(document_counter) + "', '" + cur_doc[2] + "')\"/>\n"
				document_counter += 1

			svg += "\t</g>\n\n"

	svg += "</g>"


	return svg


def svg_header(dimensions):
	return """<?xml version="1.0" encoding="UTF-8"?>
	<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

	<?xml-stylesheet type="text/css" href="svg-style.css" ?>
	<svg xmlns="http://www.w3.org/2000/svg"
	xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:ev="http://www.w3.org/2001/xml-events"
	version="1.1"
	baseProfile="full"
	width=\"""" + str(dimensions[0]) + """px"
	height=\"""" + str(dimensions[1]) + """px" > "
<rect x="0" y="0" width=\"""" + str(dimensions[0]) + """px\" height=\"""" + str(dimensions[1]) + """px\" class="border" style=\"fill:none;\" />
	"""

def coordinates_to_svg(coordinates, doc_ids = None, filename=config['WebServer']['webroot'] +'/output.svg', fixed_size=False):
	svg = generate_svg(coordinates, doc_ids, fixed_size)
	with open(filename, 'w') as out:
		out.write(svg)

def create_test_svg():
	doc_coords = [[20,50], [300,200], [100,380], [532,10], [200,150]]
	doc_ids = ["Dokument 1", "Dokument 2", "Dokument 3", "Dokument 4", "Dokument 5"]

	with open(config['WebServer']['webroot'] +'/output.svg', 'w') as out:
		out.write(generate_svg(doc_coords, doc_ids))

if __name__ == "__main__":
	create_test_svg()

