import math
from render.clusterizer import Clusterizer
from render.quick_hull import get_convex_hulls
from shapely.geometry import LinearRing
from shapely.geometry import Polygon
from shapely import affinity
import logging
from helper import config

CONST_DOCUMENT_INDICATOR_RADIUS = 1
CONST_TOPIC_TITLE_SIZE = 10
CONST_MAP_SCALE_FACTOR = 8
CONST_CLUSTERED_POINTS_CIRCLE_RADIUS = 8
CONST_OFFSET_SCALE_FST_INNER = 0.75
CONST_OFFSET_SCALE_SND_INNER = 0.4
CONST_OFFSET_SCALE_OUTER = 1.4


# doc_coords format: [[x0,y0],[x1,y1],[x2,y2],...]
# doc_titles: string array
# topic_titles: string array
# full_vectors: see indexer.get_full_vectors_for_som
def generate_svg(doc_coords, doc_titles, topic_titles, full_vectors, som=True, fixed_size=False):
	# scale the map
	if(CONST_MAP_SCALE_FACTOR != 1):
		doc_coords_scaled = []

		for i in xrange(0, len(doc_coords)):
			doc_coords_scaled.append((doc_coords[i][0] * CONST_MAP_SCALE_FACTOR, doc_coords[i][1] * CONST_MAP_SCALE_FACTOR))

		doc_coords = doc_coords_scaled

	dimensions = get_dimensons(doc_coords, fixed_size)

	# Add header
	svg = svg_header(dimensions)

	if som:
		# Add topic areas
		clusterizer = Clusterizer()
		clusterizer.clusterize(doc_coords, full_vectors)
		clusters = clusterizer.get_clusters()
		removeUnassignedTopics(clusters, topic_titles)
		hulls = get_convex_hulls(cluster_documents_to_coordinates(doc_coords, clusters))
		svg += write_topic_areas_to_svg(hulls, clusters)
		svg += "\n\n"


		# Add topic titles
		topic_centers = get_cluster_centers(hulls)
		svg += write_topic_titles_to_svg(topic_centers, topic_titles)
		svg += "\n\n"


	# Add document points
	tiles = create_tiles(doc_coords, dimensions, doc_titles)
	svg += write_document_tiles_to_svg(tiles)


	svg += "\n</svg>"

	return svg

def cluster_documents_to_coordinates(doc_coords, clusters):
	cluster_coordinates = []

	for cur_cluster in clusters:
		cur_cluster_coordinates = []

		for cur_doc in cur_cluster:
			cur_cluster_coordinates.append(doc_coords[cur_doc])

		cluster_coordinates.append(cur_cluster_coordinates)


	return cluster_coordinates


# returns the highest coordinate values
def get_dimensons(doc_coords, fixed_size=False):
	if not fixed_size is False:
		return fixed_size
	cur_dimension_x = 0;
	cur_dimension_y = 0;
	for coord in doc_coords:
		if(coord[0] > cur_dimension_x):
			cur_dimension_x = coord[0]

		if(coord[1] > cur_dimension_y):
			cur_dimension_y = coord[1]
	cur_dimension_x += CONST_DOCUMENT_INDICATOR_RADIUS*2
	cur_dimension_y += CONST_DOCUMENT_INDICATOR_RADIUS*2

	return [math.ceil(cur_dimension_x), math.ceil(cur_dimension_y)]


# sorts the documents into tiles
def create_tiles(doc_coords, dimensions, doc_titles=None, tile_size=50):
	num_tiles = [int(math.ceil(dimensions[0]/tile_size)), int(math.ceil(dimensions[1]/tile_size))]

	tiles = []
	for x in xrange(0,num_tiles[0]):
		tiles.append([])
		for y in xrange(0, num_tiles[1]):
			tiles[x].append([])

	for i in xrange(len(doc_coords)):
		if doc_titles == None:
			document_id = "foo"
		else:
			document_id = doc_titles[i]

		doc_coords_x = doc_coords[i][0] + CONST_DOCUMENT_INDICATOR_RADIUS
		doc_coords_y = doc_coords[i][1] + CONST_DOCUMENT_INDICATOR_RADIUS

		tiles[int(math.floor(doc_coords_x/tile_size))][int(math.floor(doc_coords_y/tile_size))].append((doc_coords_x, doc_coords_y,document_id))


	return tiles


# writes the previously created document tiles to the svg
def write_document_tiles_to_svg(tiles):
	svg = "<g id=\"documents\">\n"

	for x in xrange(len(tiles)):
		for y in xrange(len(tiles[x])):
			svg += "\t<g id=\"docTile_" + str(x) + "_" + str(y) + "\">\n"

			for cur_doc in tiles[x][y]:
				svg += "\t\t<rect x=\"" + str(cur_doc[0] - CONST_DOCUMENT_INDICATOR_RADIUS*1.5) + "\" y=\"" + str(cur_doc[1] - CONST_DOCUMENT_INDICATOR_RADIUS*1.5) + "\" "
				svg += "width=\"" + str(CONST_DOCUMENT_INDICATOR_RADIUS) +"\" height=\"" + str(CONST_DOCUMENT_INDICATOR_RADIUS) + "\" "
				svg += "id=\"" + cur_doc[2] + "\" class=\"docIndicators\"/>\n"

			svg += "\t</g>\n\n"

	svg += "</g>"

	return svg

def write_topic_titles_to_svg(topic_centers, topic_titles):
	svg = "<g id=\"topicTitles\">\n"

	for i in xrange(0, len(topic_centers)):
		svg += "\t<text class=\"topicTitles\" text-anchor=\"middle\" style=\"dominant-baseline: middle;font-size: " + str(CONST_TOPIC_TITLE_SIZE) + "px" + ";\" y=\"" + str(topic_centers[i][1]) + "\">\n"

		# write topice titles (each keyword in a new line)
		if i < len(topic_titles):
			split = topic_titles[i]
		else:
			split= ['foo','baar', 'IORE']

		for j in xrange(0, len(split)):
			dy = CONST_TOPIC_TITLE_SIZE
			if((j+1) < len(split) / 2.0):
				dy *= -1

			svg += "\t\t<tspan x=\"" + str(topic_centers[i][0]) + "\" dy=\"" + str(dy) + "\">" + str(split[j]) + "</tspan>\n"

		svg += "\t</text>\n"

	svg += "</g>"

	return svg


def write_topic_areas_to_svg(areas, clusters):
	# Get topic densities
	densities = []
	for i in xrange(0, len(areas)):
		densities.append(math.log(get_topic_area_density(areas[i], len(clusters[i]))))

	# Map the values to 0 - 100
	mapped_densities = map_values(densities, 0, 100)

	# Create scaled variants of the shapes
	areas_with_offsets = []

	for i in xrange(0, len(areas)):
		# Format: Circle: ["circle", density_value, center, radius] Polygon: ["polygon", density_value, points]
		outer = None
		fst_inner = None
		snd_inner = None
		unscaled = None
		if len(areas[i]) == 1 or all_documents_in_one_point(areas[i]):
			unscaled = ["circle", mapped_densities[i], areas[i][0], CONST_CLUSTERED_POINTS_CIRCLE_RADIUS]
			outer = ["circle", mapped_densities[i] - 10, areas[i][0], CONST_CLUSTERED_POINTS_CIRCLE_RADIUS*CONST_OFFSET_SCALE_OUTER]
			fst_inner = ["circle", mapped_densities[i] + 10, areas[i][0], CONST_CLUSTERED_POINTS_CIRCLE_RADIUS*CONST_OFFSET_SCALE_FST_INNER]
			snd_inner = ["circle", mapped_densities[i] + 20, areas[i][0], CONST_CLUSTERED_POINTS_CIRCLE_RADIUS*CONST_OFFSET_SCALE_SND_INNER]
		elif len(areas[i]) == 2:
			center = ((areas[i][0][0] + areas[i][1][0])*0.5, (areas[i][0][1] + areas[i][1][1])*0.5)
			unscaled = ["circle", mapped_densities[i], center, CONST_CLUSTERED_POINTS_CIRCLE_RADIUS]
			outer = ["circle", mapped_densities[i] - 10, center, CONST_CLUSTERED_POINTS_CIRCLE_RADIUS*CONST_OFFSET_SCALE_OUTER]
			fst_inner = ["circle", mapped_densities[i] + 10, center, CONST_CLUSTERED_POINTS_CIRCLE_RADIUS*CONST_OFFSET_SCALE_FST_INNER]
			snd_inner = ["circle", mapped_densities[i] + 20, center, CONST_CLUSTERED_POINTS_CIRCLE_RADIUS*CONST_OFFSET_SCALE_SND_INNER]
		else:
			unscaled = ["polygon", mapped_densities[i], areas[i]]
			outer = ["polygon", mapped_densities[i] - 10, offset_topic_area(areas[i], CONST_OFFSET_SCALE_OUTER)]
			fst_inner = ["polygon", mapped_densities[i] + 10, offset_topic_area(areas[i], CONST_OFFSET_SCALE_FST_INNER)]
			snd_inner = ["polygon", mapped_densities[i] + 20, offset_topic_area(areas[i], CONST_OFFSET_SCALE_SND_INNER)]

		areas_with_offsets.append(outer)
		areas_with_offsets.append(unscaled)
		areas_with_offsets.append(fst_inner)
		areas_with_offsets.append(snd_inner)

	# Sort all areas, so that areas with a higher density overlap areas with lower densities
	areas_with_offsets.sort(key=lambda area: area[1])


	svg = "<g id=\"topicAreas\">\n"

	for cur_area in areas_with_offsets:
		svg += write_topic_area(cur_area)

	svg += "</g>"

	return svg

def write_topic_area(area):
	svg = ""

	colour = get_topic_area_colour(area[1])

	if area[0] == "circle":
		svg += "\t<circle cx=\"" + str(area[2][0]) + "\" cy=\"" + str(area[2][1]) + "\" r=\"" + str(area[3]) + "\" style=\"fill:rgb(" + str(int(colour[0])) + "," + str(int(colour[1])) + "," + str(int(colour[2])) + ")\"/>\n"
	elif area[0] == "polygon":
		if bool(config['TopicGeneration']['svg_paths']):
			svg += "\t<path style=\"fill:rgb(" + str(int(colour[0])) + "," + str(int(colour[1])) + "," + str(int(colour[2])) + ")\" d=\""
			for i,cur_point in enumerate(area[2]):
				prefix = ""
				if i % 2 == 1:
					prefix = "Q"
				if i == 0:
					prefix = "M"
				x = cur_point[0]
				y = cur_point[1]
				if x == 0 and y == 0:
					logging.warning("Point with 0,0 omitted")
					continue
				svg += prefix + str(x) + "," + str(y) + " "
			svg += "Z\"/>\n"
		else:
			svg += "\t<polygon style=\"fill:rgb(" + str(int(colour[0])) + "," + str(int(colour[1])) + "," + str(int(colour[2])) + ")\" points=\""
			for cur_point in area[2]:
				svg += str(cur_point[0]) + "," + str(cur_point[1]) + " "
			svg += "\"/>\n"
	return svg

def offset_topic_area(topic_area, scale_factor):
	ring = LinearRing(topic_area)

	scaled_ring = affinity.scale(ring, xfact=scale_factor, yfact=scale_factor, origin="centroid")

	return list(scaled_ring.coords)

def get_topic_area_colour(density_value):
	colours = [(97,161,32), (222,213,89), (110,80,52), (197,185,174)]
	colour_thresholds = (-10.0,30.0,90.0,121.0)

	for i in xrange(0, len(colour_thresholds)):
		if density_value < colour_thresholds[i]:
			colour = []
			for j in xrange(0,3):
				colour.append(colours[i-1][j] + (colours[i][j] - colours[i-1][j]) * ((density_value - colour_thresholds[i-1])/(colour_thresholds[i] - colour_thresholds[i-1])))

			return colour

def get_topic_area_density(topic_area, num_docs_in_area):
	num_docs_in_area *= 100

	if(len(topic_area) == 1 or all_documents_in_one_point(topic_area)):
		return num_docs_in_area / (CONST_CLUSTERED_POINTS_CIRCLE_RADIUS*CONST_CLUSTERED_POINTS_CIRCLE_RADIUS*3.14)

	if(len(topic_area) == 2):
		return num_docs_in_area / (math.pow(distance(topic_area[0], topic_area[1])*0.5, 2)*3.14)

	polygon = Polygon(topic_area)

	return num_docs_in_area / polygon.area

def distance(p_1, p_2):
	return math.sqrt((p_1[0] - p_2[0])*(p_1[0] - p_2[0]) + (p_1[1] - p_2[1])*(p_1[1] - p_2[1]))

def all_documents_in_one_point(area):
	try:
		point = [area[0][0],area[0][1]]
	except IndexError:
		point = [1,1]
		print "INDEXERROR!!"

	for cur_point in area:
		if cur_point[0] != point[0] or cur_point[1] != point[1]:
			return False

	return True

def map_values(values, mapped_min, mapped_max):
	min_value = min(values)
	max_value = max(values)

	# In case this happens return a medium value for all
	if(min_value == max_value):
		max_value *= 2

	mapped_values = []

	for cur_value in values:
		mapped_values.append(mapped_min + (mapped_max-mapped_min) * ((cur_value-min_value) / (max_value-min_value)))

	return mapped_values

def get_cluster_centers(topic_areas):
	centers = []
	for cur_topic_area in topic_areas:
		if len(cur_topic_area) == 1:
			centers.append(cur_topic_area[0])
		elif len(cur_topic_area) == 2:
			x = (cur_topic_area[0][0] + cur_topic_area[1][0]) / 2
			y = (cur_topic_area[0][1] + cur_topic_area[1][1]) / 2

			centers.append((x,y))
		else:
			ring = LinearRing(cur_topic_area)
			centers.append((ring.centroid.x, ring.centroid.y))

	return centers

def removeUnassignedTopics(clusters, topic_titles):
	for i in xrange(len(clusters)-1,-1,-1):
		if(len(clusters[i]) == 0):
			del clusters[i]
			del topic_titles[i]

def svg_header(dimensions):
	return """<?xml version="1.0" encoding="UTF-8"?>
	<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

	<?xml-stylesheet type="text/css" href="svg-style.css" ?>
	<svg xmlns="http://www.w3.org/2000/svg"
	xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:ev="http://www.w3.org/2001/xml-events"
	version="1.1"
	baseProfile="full"
	width=\"""" + str(dimensions[0]) + """px"
	height=\"""" + str(dimensions[1]) + """px">
	<rect x="0" y="0" width=\"""" + str(dimensions[0]) + """px\" height=\"""" + str(dimensions[1]) + """px\" class="border" style=\"fill:none;\" />
	"""

def coordinates_to_svg(coordinates, doc_titles, topic_titles, full_vectors, filename=config['WebServer']['webroot'] +'/output.svg', fixed_size=False):
	svg = generate_svg(coordinates, doc_titles, topic_titles, full_vectors, fixed_size)
	with open(filename, 'w') as out:
		out.write(svg)

def create_test_svg():
	num_docs = 1000
	num_topics = 50

	# Read document coordinates from file
	doc_coords = []
	curX = -1
	curY = -1

	with open("bmus", 'r') as input:
		for line in input:
			if curX == -1:
				curX = float(line)
			elif curY == -1:
				curY = float(line)

			if curY != -1:
				doc_coords.append((curX, curY))
				curX = -1
				curY = -1

	# Read topc weights
	full_vectors = []

	with open("topics", 'r') as input:
		for line in input:
			split = line.split(' ')
			full_vector = []

			for curSplit in split:
				full_vector.append(float(curSplit))

			full_vectors.append(full_vector)

	# Generate document titles
	doc_titles = []

	for i in xrange(0, num_docs):
		doc_titles.append(str(i))

	# Generate topic titles
	topic_titles = []

	for i in xrange(0, num_topics):
		topic_titles.append("topic " + str(i) + "\ntopic " + str(i) + "\ntopic " + str(i))

	with open('output.svg', 'w') as out:
		out.write(generate_svg(doc_coords, doc_titles, topic_titles, full_vectors))

if __name__ == "__main__":
	create_test_svg()
