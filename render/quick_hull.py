import math


def get_convex_hulls(clusters):
	hulls = []

	for cur_cluster in clusters:
		if len(cur_cluster) < 3:
			hulls.append(cur_cluster)
			continue
		elif len(cur_cluster) == 3:
			if cur_cluster[0][0] == cur_cluster[1][0] and cur_cluster[0][1] == cur_cluster[1][1]:
				hulls.append([cur_cluster[0], cur_cluster[2]])
			elif cur_cluster[0][0] == cur_cluster[2][0] and cur_cluster[0][1] == cur_cluster[2][1]:
				hulls.append([cur_cluster[0], cur_cluster[1]])
			elif cur_cluster[1][0] == cur_cluster[2][0] and cur_cluster[1][1] == cur_cluster[2][1]:
				hulls.append([cur_cluster[0], cur_cluster[1]])
			else:
				hulls.append(cur_cluster)
			
			continue


		# Find points with lowest and highest x value
		min_x = (cur_cluster[0][0], cur_cluster[0][1])
		max_x = (cur_cluster[0][0], cur_cluster[0][1])

		for cur_point in cur_cluster:
			if cur_point[0] < min_x[0]:
				min_x = cur_point
			elif cur_point[0] > max_x[0]:
				max_x = cur_point

		# Split the points into two sets, one above and one below the line formed by the previously found points
		upper_points = []
		lower_points = []

		for cur_point in cur_cluster:
			side_of_line = get_side_of_line(min_x, max_x, cur_point)

			if side_of_line > 0:
				lower_points.append(cur_point)
			elif side_of_line < 0:
				upper_points.append(cur_point)

		# Find the hull for both parts
		upper_hull = quick_hull(min_x, max_x, True, upper_points)
		lower_hull = quick_hull(min_x, max_x, False, lower_points)

		# Merge the two hull parts
		upper_hull.insert(0, min_x)
		upper_hull.append(max_x)

		lower_hull.reverse()
		upper_hull.extend(lower_hull)

		hulls.append(upper_hull)

	return hulls

def quick_hull(line_start, line_end, points_above_line, points):
	# Get the point that is farthest away from the line
	farthest_point = None
	farthest_distance = 0

	for cur_point in points:
		side_of_line = get_side_of_line(line_start, line_end, cur_point)

		if points_above_line and side_of_line >= 0:
			continue
		elif not points_above_line and side_of_line <= 0:
			continue

		distance = get_distance_from_line(line_start, line_end, cur_point)
		if distance > farthest_distance:
			farthest_point = cur_point
			farthest_distance = distance


	# Return empty list if no point found
	if farthest_point is None:
		return []


	# Add all points that are not in the triangle formed by the line and the point just found to a new list
	points_outside_triangle = []
	for cur_point in points:
		if cur_point != farthest_point:
			if not is_point_in_triangle(cur_point, line_start, line_end, farthest_point):
				points_outside_triangle.append(cur_point)

	# Get the hull points on the left and right side
	left = quick_hull(line_start, farthest_point, points_above_line, points_outside_triangle)
	right = quick_hull(farthest_point, line_end, points_above_line, points_outside_triangle)

	# Merge the hulls
	left.append(farthest_point)
	left.extend(right)


	return left


def get_side_of_line(line_start, line_end, point):
	return (line_end[0] - line_start[0]) * (point[1] - line_start[1]) - (line_end[1] - line_start[1])*(point[0] - line_start[0])

def get_distance_from_line(line_start, line_end, point):
	return abs((line_end[1] - line_start[1]) * point[0] - (line_end[0] - line_start[0]) * point[1] + line_end[0] * line_start[1] - line_end[1] * line_start[0]) / math.sqrt(math.pow(line_end[1] - line_start[1], 2) + math.pow(line_end[0] - line_start[0],2))

def cross_product_2d(vec_1, vec_2):
	return vec_1[0] * vec_2[1] - vec_1[1] * vec_2[0]

def points_on_same_side(p_1, p_2, line_start, line_end):
	line = (line_end[0] - line_start[0], line_end[1] - line_start[1])
	vec_1 = (p_1[0] - line_start[0], p_1[1] - line_start[1])
	vec_2 = (p_2[0] - line_start[0], p_2[1] - line_start[1])

	cross_product_1 = cross_product_2d(line, vec_1)
	cross_product_2 = cross_product_2d(line, vec_2)

	return cross_product_1 * cross_product_2 >= 0

def is_point_in_triangle(point, a, b, c):
	return points_on_same_side(point, a, b, c) and points_on_same_side(point, b, a, c) and points_on_same_side(point, c, a, b)