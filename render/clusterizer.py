import math

CONST_DISTANCE_THRESHOLD = 2

class Clusterizer(object):
	def __init__(self):
		self.cluster_centers = None
		self.clusters = None


	def clusterize(self, doc_coords, full_vectors):
		num_docs = len(doc_coords)
		num_topics = len(full_vectors[0])

		# Get the best matching documents for each topic
		topic_docs = []

		for i in xrange(0, num_topics):
			topic_docs.append([])

		for i in xrange(0, num_docs):
			topic_docs[self.__get_best_topic__(full_vectors[i])].append(i)


		# Find the average distance for each node to all other nodes of the same topic
		doc_avg_distances = []

		for i in xrange(0, num_docs):
			doc_avg_distances.append(0)

		for cur_topic_docs in topic_docs:
			for cur_doc in cur_topic_docs:
				for cur_neighbour in cur_topic_docs:
					if cur_doc != cur_neighbour:
						doc_avg_distances[cur_doc] += self.__get_distance_2d__(doc_coords[cur_doc], doc_coords[cur_neighbour])

				if len(cur_topic_docs) > 1:
					doc_avg_distances[cur_doc] /= len(cur_topic_docs) - 1

		# Get the average distance for each topic
		topic_avg_distances = []

		for i in xrange(0, num_topics):
			topic_avg_distances.append(0)

		for i in xrange(0, len(topic_docs)):
			for cur_doc in topic_docs[i]:
				topic_avg_distances[i] += doc_avg_distances[cur_doc]

			if len(topic_docs[i]) > 0:
				topic_avg_distances[i] /= len(topic_docs[i])

		
		# Eliminate nodes that are too far away from other nodes of that cluster
		for i in xrange(0, len(topic_docs)):
			for j in xrange(len(topic_docs[i])-1, -1, -1):
				if doc_avg_distances[topic_docs[i][j]] > topic_avg_distances[i] * CONST_DISTANCE_THRESHOLD:
					del topic_docs[i][j]	


		self.clusters = topic_docs

		"""The cluster's centroid is now used instead
		# Get the center for each topic
		self.cluster_centers = []

		for cur_topic_docs in topic_docs:
			center = [0, 0]

			for cur_doc in cur_topic_docs:
				center[0] += doc_coords[cur_doc][0]
				center[1] += doc_coords[cur_doc][1]

			center[0] /= len(cur_topic_docs)
			center[1] /= len(cur_topic_docs)

			self.cluster_centers.append(center)
		"""


	"""The cluster's centroid is now used instead
	def get_cluster_centers(self):
		return self.cluster_centers
	"""

	def get_clusters(self):
		return self.clusters


	def __get_best_topic__(self, full_vector):
		cur_best = 0
		cur_best_index = 0

		for i in xrange(0, len(full_vector)):
			if full_vector[i] > cur_best:
				cur_best = full_vector[i]
				cur_best_index = i

		return cur_best_index


	def __get_distance_2d__(self, vec1, vec2):
		return math.sqrt(math.pow(vec1[0] - vec2[0], 2) + math.pow(vec1[1] - vec2[1], 2))