#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import os
from helper import parse_topics

def save(filename, content):
	if not filename is False:
		with open(filename, 'w') as output:
			output.write(content)

def topics2json(topics, filename=False):
	parsed_topics = parse_topics(topics)[0]
	json_topics = json.dumps(parsed_topics, sort_keys=True, indent=4, separators=(',', ': '))
	save(filename, json_topics)
	return json_topics

def documents2json(documents, filename=False):
	json_documents = json.dumps(documents, sort_keys=True, indent=4, separators=(',', ': '))
	save(filename, json_documents)
	return json_documents

if __name__ == "__main__":
	with open('_topics-gen') as input:
		print(topics2json(input.readlines()))
