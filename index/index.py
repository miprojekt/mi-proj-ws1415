#!/usr/bin/python
# -*- coding: utf-8 -*-
# author: clemens

import os, re
import logging
from xml.sax.saxutils import escape
from gensim import corpora, models, similarities

from helper import config

loglevel = int(config['General']['log_level'])
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=loglevel)

TEST_N = int(config['Indexing']['test_n'])

files = None

class FileIterator(object):
	"""Define an Iterator about all files in given directory once and store for later reuse"""
	def __init__(self, name, limit=TEST_N):
		self._name = name
		self._files = None
		self.limit = limit

	def __iter__(self):
		global files
		if self._files is None and not files is None:
			logging.debug("reuse of global files")
			self._files = files
		elif self._files is None:
			self._files = self._iterate_dir_(self._name)
			logging.info("iterated "+ str(len(self._files)) + " files")
			if files is None:
				logging.debug("save iterated files to global files")
				files = self._files
		for file in self._files:
			yield file

	def _iterate_dir_(self, name):
		logging.debug("iterate dir to find all files in collection")
		files = []
		stop = False
		for root, dirs, fils in os.walk(name):
			logging.debug("in path "+root)
			for name in fils:
				filename = os.path.join(root, name)
				logging.debug("file: "+filename)
				files.append(filename)
				stop = len(files) >= self.limit
				if stop:
					logging.warning("reached max document limit of "+str(self.limit))
					break
			if stop:
				break
		return files

class FileReader(object):
	"""Iterator for a list of files, returning the contents of each"""
	def __init__(self, files):
		self.files = files

	def __iter__(self):
		for file in self.files:
			try:
				yield self.__read__(file)
			except UnicodeDecodeError as e:
				logging.error("UnicodeDecodeError happend on file " + file)
	def __read__(self, file):
		with open(file, 'r') as input:
			return input.read().decode('utf8').strip()
		

class CollectionFolder(object):
	"""define a collection"""
	def __init__(self, collection_info):
		self.collection_info = collection_info
		self.source = FileIterator(collection_info[2])

	def __iter__(self, tokenize=True):
		for document in FileReader(self.source):
			yield self.tokenize(document) if tokenize else document

	def tokenize(self, string):
		tokens = string.split()
		not_empty = lambda x: not x == ""
		stripped = map(self.treat_token, tokens)
		return filter(not_empty, stripped)

	def treat_token(self, token):
		# TODO: maybe moar limits
		token = token.strip(",.-")
		return token.lower()

	def documents(self):
		contents = iter(self.__iter__(False))
		for document in self.source:
			path = document.split("\n")[0]
			id = escape(path)
			id = re.sub('[^\w]', '', id)
			for content in FileReader([path]):
				#content = contents.next()
				title = escape(content.split("\n")[0].strip())
				yield Document(title = title, id = id, path=path)

class Document(object):
	def __init__(self, id, content = [], title = "", vector = {}, path=""):
		self.content = content
		self.title = title
		self.vector = vector
		self.id = id
		self.path = path

class Collection(object):
	""" Load documents into corpus without holding them all in memory"""
	def __init__(self, dictionary, collection):
		logging.debug("collection: __init__()")
		self.dictionary = dictionary
		self.folder = CollectionFolder(collection)

	def __iter__(self):
		for document in self.folder:
			logging.debug("collection: __iter__:"+str(document))
			yield self.dictionary.doc2bow(document)

	def documents(self):
		return self.__iter__()

	def contents(self):
		for document in self.folder.documents():
			yield document.content

	def documents_meta(self):
		for document in self.folder.documents():
			yield document

class Index(object):
	""" Handle indexing and creation of vectors """
	def __init__(self):
		self.vectors = None
		self.dictionary = None
		self.collection_info = None
		self.stopwords = []
		stopwordfiles = config['Indexing']['stopwords']
		for s in stopwordfiles:
			self.__load_stopwords__(s)
		logging.info("using " + str(len(self.stopwords)) + " stopwords")

	def __load_stopwords__(self,file):
		"""load files with stopwords into stopwordlist (multiple files are possible)"""
		with open(file) as input:
			for line in input:
				new_words = line.split(",")
				self.stopwords += new_words

	def index(self, collection):
		""" create dictionary """
		self.collection_info = collection
		collection = CollectionFolder(collection)
		self.dictionary = corpora.Dictionary(document for document in collection)
		stop_ids = [self.dictionary.token2id[stopword] for stopword in self.stopwords if stopword in self.dictionary.token2id]
		once_ids = [tokenid for tokenid, docfreq in self.dictionary.dfs.iteritems() if docfreq == 1]
		self.dictionary.filter_tokens(stop_ids + once_ids)
		self.dictionary.compactify()
		#self.dictionary.save("dict.dict")
		#corpora.MmCorpus.serialize('corpus.mm', LoadCorpus(self.dictionary))

	def vectorize_iter(self, force=False):
		""" transform corpus to feature-vector of format [(feature, value)]"""
		logging.debug("Index: vectorize_iter()")
		if not self.vectors is None and not force:
			logging.debug("Index: vectorize_iter: ! self.vectors != None: "+str(self.vectors))
			vectors = self.vectors
		else:
			logging.debug("Index: vectorize_iter: ! self.vectors == None")
			vectors = Collection(self.dictionary, self.collection_info)
		for vector in vectors:
			yield vector

	def vectorize(self):
		logging.debug("Index: vectorize()")
		if self.vectors is None:
			logging.debug("Index: vectorize: ! self.vectors == None")
			self.vectors = []
			for vector in self.vectorize_iter(force=True):
				self.vectors.append(vector)
		else:
			logging.debug("Index: vectorize: ! self.vectors != None")
		return self.vectors

	def documents_meta(self, dictionary=None, collection=None):
		if dictionary is None:
			dictionary = self.dictionary
		if collection is None:
			collection = self.collection_info
		meta = {}
		for document in Collection(dictionary, collection).documents_meta():
			meta[document.id] = (document.title, document.path)
		return meta

	def tokencount(self):
		max = 0
		token = self.dictionary.token2id
		for key in token:
			if token[key] > max:
				max = token[key]
		return max

