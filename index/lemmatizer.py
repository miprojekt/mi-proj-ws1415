import os
from gensim import utils
from helper import *

def lemmatize_collection(path):
	if path[len(path)-1:] == "/":
		path = path[:len(path)-1]


	if os.path.exists(path + "-lemmatized"):
		return False


	for root, dirs, files in os.walk(path):
		for name in files:
			relative_path = root.replace(path + "/", "")
			new_file_name = os.path.join(path + "-lemmatized", relative_path, name)

			with open(os.path.join(root, name), "r") as file:
				lemmas = []
				for line in file:
					lemmas += utils.lemmatize(line)

				lem_content = ""
				for lemma in lemmas:
					tag = lemma[lemma.rfind("/")+1:]
					if tag in config["Indexing"]["lemma_tags"]:
						lem_content += lemma[:lemma.rfind("/")] + " "

				lem_content = lem_content[:len(lem_content)-1]

				if not os.path.exists(os.path.dirname(new_file_name)):
					os.makedirs(os.path.dirname(new_file_name))

				with open(new_file_name, "w+") as lem_file:
					lem_file.write(lem_content)

	return True