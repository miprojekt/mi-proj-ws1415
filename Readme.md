# Requirements

Python 2.7

##	Required libraries

* gensim [http://radimrehurek.com/gensim/index.html](http://radimrehurek.com/gensim/index.html)
	* numpy
	* scipy
* pattern [http://www.clips.ua.ac.be/pattern](http://www.clips.ua.ac.be/pattern)
* somoclu [https://peterwittek.github.io/somoclu/](https://peterwittek.github.io/somoclu/)
* shapely [http://toblerity.org/shapely/](http://toblerity.org/shapely/)

Legacy SOM

* PyMVPA2
	* python-mvpa2
	* https://github.com/PyMVPA/PyMVPA + sudo apt-get install swig + python setup.py build_ext + sudo python setup.py install
* pylab
	* python-matplotlib

###	Installation of dependencies with Ubuntu:

```
#!bash
sudo apt-get install python-dev python-pip libgeos-dev python-numpy python-scipy screen
sudo pip install gensim
sudo pip install pattern
sudo pip install somoclu
sudo pip install shapely
```

###	Installation of dependencies with Windows:

* Downloads:
	* Python 2.7.9: https://www.python.org/downloads/release/python-279/
		* https://www.python.org/ftp/python/2.7.9/python-2.7.9.amd64.msi
		* versions prior to 2.7.9 don't have pip included
	* numpy, scipy, shapely: When installed via pip, they need many libs and compilers, so use of a prebuilt wheel is more feasible
		* About wheels: https://pip.pypa.io/en/latest/user_guide.html#installing-from-wheels
		* numpy: http://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy
			* numpy-1.9.2+mkl-cp27-none-win_amd64.whl
		* scipy: http://www.lfd.uci.edu/~gohlke/pythonlibs/#scipy
			* scipy‑0.15.1‑cp27‑none‑win_amd64.whl
		* shapely: http://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely
			* Shapely‑1.5.7‑cp27‑none‑win_amd64.whl
* Consider this regarding possible ImportErrors with somoclu:
	* https://peterwittek.github.io/somoclu/#windows

```
python-2.7.9.amd64.msi
cd <python-dir>/Scripts
pip install <numpy-download>
pip install <scipy-download>
pip install <shapely-download>
pip install gensim
pip install pattern
pip install somoclu
```

#	Topic modelling

##	Generate Topicmap

Run `python topic_map_generator.py`

# Webserver

Run `python web.py`

The server will listen on port 8080 (8000 or 8081 if 8080 is not free)

