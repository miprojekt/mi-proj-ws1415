#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging
import somoclu
import numpy as np
from helper import config

def run_somoclu(full_vectors):
	data = np.array(full_vectors)
	data = np.float32(data)
	nSomX = int(config["TopicGeneration"]["som_x"])
	nSomY = int(config["TopicGeneration"]["som_y"])
	nVectors = data.shape[0]
	nDimensions = data.shape[1]
	data1D = np.ndarray.flatten(data)
	nEpoch = int(config["TopicGeneration"]["num_epochs"])
	radius0 = 0
	radiusN = 0
	radiusCooling = "linear"
	scale0 = 0
	scaleN = 0.01
	scaleCooling = "linear"
	kernelType = 0
	mapType = "planar"
	snapshots = 0
	initialCodebookFilename = ''
	codebook_size = nSomY * nSomX * nDimensions
	codebook = np.zeros(codebook_size, dtype=np.float32)
	globalBmus_size = int(nVectors * int(np.ceil(nVectors/nVectors))*2)
	globalBmus = np.zeros(globalBmus_size, dtype=np.intc)
	uMatrix_size = nSomX * nSomY
	uMatrix = np.zeros(uMatrix_size, dtype=np.float32)

	logging.info("start som training")

	somoclu.trainWrapper(data1D, nEpoch, nSomX, nSomY,
		                 nDimensions, nVectors,
		                 radius0, radiusN,
		                 radiusCooling, scale0, scaleN,
		                 scaleCooling, snapshots,
		                 kernelType, mapType,
		                 initialCodebookFilename,
		                 codebook, globalBmus, uMatrix)

	logging.info("finished som training")

	doc_coordinates = []
	for i in xrange(0,globalBmus_size,2):
		doc_coordinates.append((globalBmus[i], globalBmus[i+1]))
	return doc_coordinates
