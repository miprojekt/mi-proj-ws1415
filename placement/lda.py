#!/usr/bin/python2
# -*- coding: utf-8 -*-

import logging

from gensim.models.ldamodel import LdaModel

from index.index import Collection
from helper import config

class LDA(object):
	"""get LDA-calculated topic-document mapping"""
	def __init__(self, indexer):
		self.__indexer__ = indexer
		self.lda = None

	def __new_collection__(self):
		return Collection(self.__indexer__.dictionary, self.__indexer__.collection_info)

	def build_ldamodel(self, num_topics=10, writeFile=False):
		collection = self.__new_collection__()
		passes = int(config["TopicGeneration"]["lda_passes"])
		iterations = int(config["TopicGeneration"]["lda_iterations"])
		self.lda = LdaModel(collection, num_topics=num_topics, passes=passes, iterations=iterations,  id2word=self.__indexer__.dictionary)
		topics = self.lda.print_topics(num_topics)
		if writeFile:
			with open('_topics-gen','w') as out:
				out.write('\n'.join('{}'.format(k) for k in topics))
		return topics

	def __get_val_safe__(self, source, index):
		result = []
		fail = False
		for i in index:
			try:
				result.append(source[i][1])
			except IndexError:
				logging.warning("IndexError (X) on "+str(i))
				fail = True
		return result[0], result[1], fail

	def doc_topic_mapping(self, num_topics=10, dim1=0, dim2=1, size= (1000, 1000)):
		collection = self.__new_collection__()
		vect = []
		n = 0
		f = 0
		dims = [dim1, dim2]
		for doc in collection:
			x, y = 0, 0
			doc_lda = self.lda.__getitem__(doc, 0)
			val_x, val_y, fail = self.__get_val_safe__(doc_lda, dims)
			x = val_x * size[0]
			y = val_y * size[1]
			if fail:
				f+=1
			vect.append((x, y))
			n+=1
		logging.debug(str(f)+ "/"+str(n)+" files failed, thats " + str(f/float(n)*100.0) + "%!")
		return vect

	def get_full_vectors_for_som(self, num_topics=10):
		collection = self.__new_collection__()
		full_vectors = []
		i = 0
		for doc in collection:
			full_vector = []
			for i in xrange(0,num_topics):
				append_null = True
				for vector in self.lda[doc]:
					if vector[0] == i:
						full_vector.append(vector[1])
						append_null = False
						break
				if append_null:
					full_vector.append(0)
			full_vectors.append(full_vector)
		return full_vectors
