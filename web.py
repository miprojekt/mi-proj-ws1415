#!/usr/bin/env python
# -*- coding: utf-8 -*-
import SimpleHTTPServer
import SocketServer
import socket
import os

from helper import config, rec_int

PORTS = rec_int(config['WebServer']['ports'])

def run():
	os.chdir(config['WebServer']['webroot'])
	handler = SimpleHTTPServer.SimpleHTTPRequestHandler
	for port in PORTS:
		try:
			httpd = SocketServer.TCPServer(("", port), handler)
			try:
				print("serving at port " + str(port))
				httpd.serve_forever()
			except KeyboardInterrupt:
				print(" KeyboardInterrupt catched, exiting")
				break
		except socket.error:
			pass

if __name__ == "__main__":
	run()
