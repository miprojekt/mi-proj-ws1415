#!/usr/bin/python2
# -*- coding: utf-8 -*-
# author: clemens

import logging
import os
import sys

from helper import *

parse_args()

loglevel = int(config['General']['log_level'])
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=loglevel)
# DEBUG(10) - INFO(20) - WARNING(30) - ERROR(40) - CRITICAL(50)

logging.info("load imports")

from placement import PlacementStrategy
from index.index import Index, Collection
from index.lemmatizer import lemmatize_collection
from placement.lda import LDA
from placement.lda_som import run_somoclu
from render.svg_generator import coordinates_to_svg
from render.simple_svg_generator import coordinates_to_svg as simple_svg
from render.html import topics2json, documents2json

logging.info("imports done")
logging.info("load collections")

collections = config['Collections']['collections']
num_topics = int(config['TopicGeneration']['num_topics'])
resolution = (
		int(config['TopicGeneration']['resolution_x']),
		int(config['TopicGeneration']['resolution_y'])
	)
strategies = rec_int(config['TopicGeneration']['strategies'])
webroot = config['WebServer']['webroot']
titles_json_path = webroot + "/" + config['TopicGeneration']['titles_json_path']
topics_json_path =webroot + "/" + config['TopicGeneration']['topics_json_path']
build_all_dir = webroot + "/" + config['TopicGeneration']['all_path']
build_all = bool(config['TopicGeneration']['build_all'])

first_collection = collections[0]
if first_collection == ():
	logging.critical("no collection found, aborting...")
	sys.exit(1)
logging.info("start indexing collection "+str(first_collection) + " ("+str(first_collection)+")")

if not os.path.exists(webroot):
	os.makedirs(webroot, 0000755)

indexer = Index()

# Lemmatize collection
if bool(config["Indexing"]["lemmatize"]):
	logging.info("start lemmatizing " + first_collection[1])
	if lemmatize_collection(first_collection[2]):
		logging.info("finished lemmatzing " + first_collection[1])
	else:
		logging.info(first_collection[1] + " already lemmatized")
	source_collection = first_collection
	first_collection = (first_collection[0], first_collection[1], first_collection[2] + "-lemmatized", first_collection[3])
	# meta-information should be unlemmatized
	indexer.index(source_collection)
	doc_metas = indexer.documents_meta(dictionary=None, collection=source_collection)
	indexer = Index()
	indexer.index(first_collection)
else:
	# or not
	indexer.index(first_collection)
	doc_metas = indexer.documents_meta()

documents2json(doc_metas, titles_json_path)
doc_ids = meta2ids(doc_metas)

logging.info("perform placement")

lda = LDA(indexer)
topics = lda.build_ldamodel(num_topics=num_topics)

if PlacementStrategy.LDA in strategies:
	logging.info("using lda model")
	if build_all:
		logging.info("drawing all possible combinations")
		if not os.path.exists(build_all_dir):
			os.mkdir(build_all_dir, 0000755)
		for i in range(0, num_topics):
			for j in range(0, num_topics):
				coordinates = lda.doc_topic_mapping(num_topics=num_topics, dim1=i, dim2=j, size=resolution)
				simple_svg(coordinates, doc_ids, build_all_dir +"/"+ str(i) +"-"+ str(j) +".svg", fixed_size=resolution, som=False)
		topics2json(topics, topics_json_path)
		print(topics)
	else:
		logging.info("drawing only topics 0-1")
		coordinates = lda.doc_topic_mapping(num_topics=num_topics, size=resolution)
		simple_svg(coordinates, doc_ids)

if PlacementStrategy.LDA_SOM in strategies:
	logging.info("using lda and som")
	full_vectors = lda.get_full_vectors_for_som(num_topics=num_topics)
	coordinates = run_somoclu(full_vectors)
	coordinates_to_svg(coordinates, doc_ids, parse_topics(topics,3)[1], full_vectors, fixed_size=resolution)

if PlacementStrategy.SOM in strategies:
	logging.critical("using legacy placement strategy SOM, consider using a better one...")
	raw_vectors = indexer.vectorize()
	full_vectors = raw2full(raw_vectors, indexer.tokencount())
	coordinates = run_somoclu(full_vectors)
	svg = simple_svg(coordinates)
	with open(webroot+"/output.svg", "w") as out:
		out.write(svg)
	
logging.info("done")
