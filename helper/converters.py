#!/usr/bin/python
# -*- coding: utf-8 -*-

def rec_int(list):
	return map(int, list)

def meta2ids(meta):
	return [k for k in meta]

def parse_topics(topics, n=False):
	words=[]
	new_topics = []
	for line in topics:
		tokens=line.split('+')
		topic=[]
		for token in tokens:
			elem=token.split('*')
			topic.append(elem[1].strip().encode('ascii','ignore'))
			if n:
				if n == len(topic):
					break
		string=", ".join(topic)
		words.append([string])
		new_topics.append(topic)
	return words, new_topics

def raw2full_iter(raw_vectors, count):
	for raw in raw_vectors:
		full = [0] * (count +1)
		for feature in raw:
			i = feature[0]
			value = feature[1]
			full[i] = value
			feature = []
		yield full
		raw = []
	raw_vectors = None
	
def raw2full(raw_vectors, count):
	full_vectors = []
	for full in raw2full_iter(raw_vectors, count):
		full_vectors.append(full)
	return full_vectors

