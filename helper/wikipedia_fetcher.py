import wikipedia, os, sys, re, random, time
from threading import Thread


def save_article(article, folder, depth):
	try:
		page = wikipedia.page(title=article)
	except wikipedia.exceptions.DisambiguationError as e:
		page = wikipedia.page(title=e.options[0])

	try:
		with open(os.path.join(folder, page.title + ".txt"), "w") as output:
			output.write(re.sub("[^\w\s]", "", page.title + "\n"))
			output.write(re.sub("[^\w\s]", "", page.content))

		if depth > 0:
			links_followed = 0
			for cur_link in [page.links[i] for i in random.sample(xrange(len(page.links)),10)]:
				Thread(target=save_article, args=(cur_link[cur_link.rfind("/")+1:], folder, depth-1)).start()
				time.sleep(0.5)
				links_followed += 1
				if links_followed >= 10:
					break
	except IOError:
		# If same file is accessed by two threads
		pass

if __name__ == "__main__":
	if len(sys.argv) == 1:
		print("Invoke as follows: python wikipedia_fetcher <start_page> [<dest_folder>] [<link_depth>]")
	else:
		start_page = sys.argv[1]
		dest_folder = "htdocs/data/wikipedia-fetched"
		depth = 0

		if len(sys.argv) >= 3:
			depth = int(sys.argv[2])

		if len(sys.argv) >= 4:
			dest_folder = sys.argv[3]

		os.makedirs(dest_folder)
		save_article(start_page, dest_folder, depth)
