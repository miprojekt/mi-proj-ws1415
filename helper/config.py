#!/usr/bin/python3
# -*- coding: utf-8 -*-

import ConfigParser
import os
import argparse
from converters import rec_int
import logging

def __parse_collection__(collection):
	i, name, path, comment = collection.strip().split(",")
	if os.path.exists(path):
		return (i, name, path, comment)
	return ()

def read_config():
	for section in config_read.sections():
		options = {}
		for option in config_read.options(section):
			val = config_read.get(section, option)
			if '-' in option:
				name = option[:option.find('-')]
				if section == "Collections":
					val = __parse_collection__(val)
				if not options.has_key(name):
					options[name] = [val]
				else:
					options[name] += [val]
			else:
				if val == "False":
					options[option] = False
				else:
					options[option] = val
		config[section] = options

def parse_args():
	parser = argparse.ArgumentParser(description="Generate TopicMap")
	parser.add_argument("-c", "--collection", help="collection (csv-entry)", default=config['Collections']['collections'][0], required=False)
	parser.add_argument("-p", "--placement", help="Placement strategy", default=rec_int(config['TopicGeneration']['strategies'])[0], required=False)
	parser.add_argument("-n", "--documents", help="Document count", default=config['Indexing']['test_n'], required=False)
	parser.add_argument("-i", "--iterations", help="SOM iterations", default=config['TopicGeneration']["num_epochs"], required=False)
	parser.add_argument("-o", "--output", help="Output folder", default=config["WebServer"]["webroot"], required=False)
	parser.add_argument("-s", "--svg-paths", help="Use SVG-paths", default=config["TopicGeneration"]["svg_paths"], action="store_false")
	parser.add_argument("-v", "--verbose", help="set logging to CRITICAL", action="store_true")
	args = parser.parse_args()
	config['Collections']['collections'][0] = args.collection
	config['TopicGeneration']['strategies'][0] = args.placement
	config['Indexing']['test_n'] = args.documents
	config['TopicGeneration']["num_epochs"] = args.iterations
	config["WebServer"]["webroot"] = args.output
	config["TopicGeneration"]["svg_paths"] = args.svg_paths
	if args.verbose:
		config['General']['log_level'] = logging.DEBUG

config_read = ConfigParser.ConfigParser()
config_read.read('config.ini')
config = {}
read_config()	# Call config-reader

