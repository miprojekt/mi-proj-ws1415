#!/usr/bin/ruby
# -*- coding: utf-8 -*-

Dir["data/wikipedia/original/*.txt"].each do |f|
  name = File.basename f, ".txt"
  content = File.read(f)
  100.times do |i|
    start = rand(content.size)
    limit = start + rand(20)
    puts id = "#{name}-#{i}"
    variation = "#{id}\n"
    variation += "#{content[0..start]}#{content[limit..-1]}"
    num = if i < 10 then "0" + i.to_s else i end
    File.write("data/wikipedia/#{name}-#{i}.txt", variation)
  end
end